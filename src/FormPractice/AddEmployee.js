import React, {Component} from 'react';
import './Form.css'
import DisplayEmployee from './DisplayEmployee';
export default class AddEmployee extends Component {
    constructor(props){
        super(props);
        this.state = {
            employee:{
            Id:'',
            Name:'',
            Location:'',
            Salary:''
            },
        };
    }

    changeHandler=e=>{
        const name = e.target.name;
        const value = e.target.value;
        this.setState({employee: {
            ...this.state.employee, [name]: value
        }})
    }

    onCreateEmployee=()=>{
        console.log("emp", this.state)
       let details = {...this.state.employee}
       return details;
    }

    render() {
        return (
            <>
            <div className ="box">
            <h2> Add New Employee...</h2>
            <hr className = "line"></hr>
            <form>
            <p>
                <label className="labels">Employee ID : <input type="text" name="Id" value={this.state.employee.Id} 
				       onChange={this.changeHandler} ></input>
	            </label>
	            <label className="labels">Employee Name : <input type="text" name="Name" 
                       value={this.state.employee.Name} onChange={this.changeHandler}></input>
	            </label>
	            <label className="labels">Employee Location : <input type="text" name="Location" 
                        value={this.state.employee.Location} onChange={this.changeHandler} ></input>
	            </label>
                <label className="labels">Employee Salary : <input type="number" name="Salary"
                        value={this.state.employee.Salary} onChange={this.changeHandler}></input>
                </label>
            </p>
            </form>
            <button className="add" onClick={this.onCreateEmployee}>Add Employee</button>
            </div>
            <DisplayEmployee employeeData = {this.state.employee} />
            </>
        )
    }
}