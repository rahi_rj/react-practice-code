import React, {Component} from 'react'
import './Form.css'

export default class DisplayEmployee extends Component {
constructor(props){
    super(props)
}

render(){
    return (
        <>
         <div className ="box">
            <h2> Data Entered in the above form...</h2>
            <hr className = "line"></hr>            
            <h5 className = "info">Employee ID: {this.props.employeeData.Id}</h5>
            <h5 className = "info">Employee Name: {this.props.employeeData.Name}</h5>
            <h5 className = "info">Employee Location: {this.props.employeeData.Location}</h5>
            <h5 className = "info">Employee Salary: {this.props.employeeData.Salary}</h5>
         </div>                
        </>
    )
}
}