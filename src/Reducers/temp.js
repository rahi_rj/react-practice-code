const initialState = {
    "name" : 'Rahul',
    "company" : ''
}

export default (state = initialState,action) => {
    switch(action.type) {
        case "UPDATE_NAME" : return {...state, 'name' : action.payload}
        default : return state
    }
}

// A reducer is a function that receives the current state and an action object, decides how to update the state if necessary, and returns the new state:
// A Redux reducer function is exactly the same idea as this "reduce callback" function! It takes a "previous result" (the state), and the "current item" (the action object), decides a new state value based on those arguments, and returns that new state.
// Reducers act like event listeners, and when they hear an action they are interested in, they update the state in response.

// Reducers must always follow some specific rules:
// 1. They should only calculate the new state value based on the state and action arguments
// 2. They are not allowed to modify the existing state. Instead, they must make immutable updates, by copying the existing state and making changes to the copied values.
// 3. They must not do any asynchronous logic, calculate random values, or cause other "side effects"