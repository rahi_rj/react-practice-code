import React,{Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {updateName} from './../actions/tempAction'

class TempComp extends Component {

    updateNameHandler = () => {
        this.props.updateName('Manisha')
    }

    render(){
        return(<>
            <p>{this.props.tempdata.name}</p>
            <button onClick={() => this.updateNameHandler()}>Update Name</button>        
        </>)
    }

}

const mapStateToProps = state => {
    return {tempdata : state.tempReducer}
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({updateName}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(TempComp);
