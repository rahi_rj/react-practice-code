import React from 'react';
import { BrowserRouter } from 'react-router-dom';

/** importing all the modules */
import './App.css';
// import Name from './name';
 import HeaderComponent from './Header/HeaderComponent';
// import OrderComponent from './LiftingUpStatePractice/OrderComponent';
// import LifeCyclePractice from './LifeCycleMethodPractice/LifeCyclePractice';
// import FormHeaderComponent from './FormHeaderComponent/FormHeaderComponent';
// import EmployeeHeaderComponent from './EmployeeHeaderComponent/EmployeeHeaderComponent';
// import LifeCyclePractice from './LifeCycleMethodPractice/LifeCyclePractice';
// import AdminDashboard from './HOC Concepts/AdminDashboard'
// import AdminDashboardHOC from './HOC Concepts/AdminDashboardUsingHOC'
// import FunctionalComponent from './Functional Components Concepts/usestateConcept'
// import FunctionalComponent from './Functional Components Concepts/useEffectConcept'
// import FunctionalComponent from './Functional Components Concepts/useContext'
import Routing from './Routing Concepts/routing'
// import TempComp from './tempComp/tempComp'


// const printAlert = () => alert('Bi-directional click happened');

function App() {
  return (

    // <div className="App">
    //   <Name tempname="check" showMsg={printAlert} />
    // </div>

    <BrowserRouter>

    <HeaderComponent />    

    {/* Basic concept of state and props practice */}
    {/* <EmployeeHeaderComponent />  */}

    {/* Concept of Form practice */}
    {/* <FormHeaderComponent />   */}

    {/* Concept of Lifting of state practice */}    
    {/* <OrderComponent />   */}

    {/* Concept of HOC */}
    {/* <AdminDashboard /> */}
    {/* <AdminDashboardHOC /> */}

    {/* Concept of Life Cycle Methods of class based Component */}
    {/* <LifeCyclePractice /> */}

    {/* Concept of Functional Component implementing Hooks */}
    {/* <FunctionalComponent /> */}

    {/* Concept of Routing */}
    <Routing />

    {/* Concept of Redux */}
    {/* <TempComp /> */}

    </BrowserRouter>
  );
}

export default App;



