import React, {Component} from 'react';

class Name extends Component {
    constructor(props) {
        super(props)
    }

    state = {
        name : this.props.tempname
    }

    updateName =(e) =>{
        this.setState({name : e.target.value})
    }

    render() {
        return(
            <>
                My name is <input value={this.state.name} onChange={(e) => this.updateName(e)} />
                <button onClick={this.props.showMsg}>Bi-Directional Functionality</button>
            </>
        );
    }

}


//React
//React Virtual DOM
//Props State
//Binding 
//Class component and functional Component
//State vrs Stateless component
//life cycle 


export default Name;