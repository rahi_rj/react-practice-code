import React from 'react';
import {Table} from 'react-bootstrap';
import './styles.css';

export default class DeptReports extends React.Component {

    constructor(props) {
  
      super(props);
  
      this.state = {
  
        dept: []
  
      };
  
    }
  
    componentDidMount() {
  
    //   fetch("https://localhost:44306/api/Dept")
  
    //     .then(res => res.json())
  
    //     .then(
  
    //       (result) => {          
  
    //         this.setState({
  
    //           dept: result
  
    //         });
  
    //       }
  
    //     );

        var result = [
            {
              "Id": 1,
              "Name": "FullStack",
              "Revenue": 30000
            },
            {
                "Id": 2,
                "Name": "WebDeveloper",
                "Revenue": 30000
            }
          ];

          this.setState({
  
            dept: result

          });
  
    }


    render() {
  
      return (
        <div className="tableData">
            <div>
                <h2>Department Details...</h2>
            </div>
        <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                    <th> Department Id</th>
                    <th> Department Name</th>
                    <th> Department Revenue</th>
                    </tr>
                </thead>
                <tbody>                  
                    {this.state.dept.map(department => (  
                    <tr key={department.Id}>
                        <td>{department.Id}</td>
                        <td>{department.Name}</td>
                        <td>{department.Revenue}</td>
                    </tr>
                    ))}
                </tbody>
        </Table> 
        </div>  
        );
  
      }
  
  }
  