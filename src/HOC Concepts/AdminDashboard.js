import React from 'react';
import EmployeeReports from './EmployeeReports'
import DeptReports from './DepartmentReports'

export default class AdminDashboard extends React.Component{

    constructor(props){
  
      super(props);
  
    }
  
    render(){
  
      return (
  
        <React.Fragment>
  
          <EmployeeReports></EmployeeReports>
  
          <DeptReports></DeptReports>
  
        </React.Fragment>
  
      );
  
    }
  
  }