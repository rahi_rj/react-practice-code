import React from 'react';
import {Table} from 'react-bootstrap';
import './styles.css';

export default class EmployeeReports extends React.Component {

    constructor(props) {
  
      super(props);
  
      this.state = {
  
        employees: []
  
      };
  
    }
  
    componentDidMount() {
  
    //   fetch("https://localhost:44306/api/Employee")
  
    //     .then(res => res.json())
  
    //     .then(
  
    //       (result) => {
  
    //         this.setState({
  
    //           employees: result
  
    //         });
  
    //       }
  
    //     );

        var result = [
            {
              "Id": "M1044334",
              "Name": "Rahi",
              "Location": "Bangalore",
              "Salary": 5000
            },
            {
                "Id": "M1044335",
                "Name": "Rahul",
                "Location": "Bangalore",
                "Salary": 6000
              }

          ];
          this.setState({
  
            employees: result

          });
  
    }
  
    render() {
  
      return (  
        <div className="tableData">
            <div>
                <h2>Employee Details...</h2>
            </div>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                    <th> Employee Id</th>
                    <th> Employee Name</th>
                    <th> Employee Location</th>
                    <th> Employee Salary</th>
                    </tr>
                </thead>
                <tbody>                  
                    {this.state.employees.map(emp => (  
                    <tr key={emp.Id}>
                        <td>{emp.Id}</td>
                        <td>{emp.Name}</td>
                        <td>{emp.Location}</td>
                        <td>{emp.Salary}</td>
                    </tr>
                    ))}
                </tbody>
            </Table> 
        </div>
        );
  
      }
  
  }