import React from "react";
import {Table} from 'react-bootstrap'
import './styles.css'
function reportsHOC(InputComponent, inputData) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data:[],
        columns:inputData.columns,
        header:inputData.header
      };
    }

    componentDidMount() {
      var result = inputData.info;
      this.setState({
        data: result
      });
    }

    render() {
      return (
      <Data data={this.state}></Data>
      );
    }
  };
}

class Data extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return (
    <div className="tableData">
        <h2>{this.props.data.header}...</h2>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr>
            {this.props.data.columns.map(c => (
              <th>{c}</th>
            ))}
            </tr>
          </thead>
          <tbody>
            {this.props.data.data.map(emp => (
              <tr key={emp.Id}>
                {this.props.data.columns.map(c => (
                  <td>{emp[c]}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </Table>
    </div>
    );
  }

}

class Reports extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return(
      <div></div>
    );
  }
}

const EmployeeReports = reportsHOC(Reports,{
    info:[
        {
            "Id": "M1044334",
            "Name": "Rahi",
            "Location": "Bangalore",
            "Salary": 5000
        },
        {
            "Id": "M1044335",
            "Name": "Rahul",
            "Location": "Bangalore",
            "Salary": 6000
        }
    ], 
    columns:['Id','Name','Location','Salary'],
    header:'Employee Data'
});


const DeptReports = reportsHOC(Reports,{
    info: [
            {
            "Id": 1,
            "Name": "FullStack",
            "Revenue": 30000
            },
            {
            "Id": 2,
            "Name": "WebDeveloper",
            "Revenue": 30000
            }
    ],
    columns:['Id','Name','Revenue'],
    header:'Department Data'
});


export default class AdminDashboardHOC extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return (
      <>
        <EmployeeReports></EmployeeReports>
        <DeptReports></DeptReports>
      </>
    );
  }
}