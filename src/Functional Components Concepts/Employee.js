import React, { useState } from 'react';
import Salary from './Salary';
import './style.css';

export default function EmployeeComponent () {
    // const [name, setName] = useState();
    // const [name, setName] = useState('Rahul Jha'); // for default value
    const [employee,setEmployeeData]=useState({Id:'',Name:'',Location:'',Salary:''}); 

    // function changeName(e){
    //     setName(e.target.value);
    // }

    function changeEmployeeInfo(e){
        setEmployeeData({...employee,[e.target.name]:e.target.value});
      }

    return(

        // <div className = 'box'>    
        //   <h2>Welcome to New Employee Functional Component...</h2> 
        //   <hr className = "line"></hr>   
        //   <p>    
        //     <label>Employee Name :     
        //             <input type="text" value={name}     
        //             onChange={changeName}></input>    
        //     </label>    
        //   </p>    
        //   <p>    
        //     Entered Name is : <b>{name}</b>
        //   </p>    
        // </div> 

        <>
        <div className = 'box'>    
          <h2>Employee Functional Component...</h2> 
          <hr className = "line"></hr>   
            <p>
                <label className="labels">Employee ID :
                    <input type="text" name="Id" value={employee.Id}
                    onChange={changeEmployeeInfo}></input>
                </label>
                <label className="labels">Employee Name : 
                        <input type="text" name="Name" value={employee.Name} 
                        onChange={changeEmployeeInfo}></input>
                </label>
                <label className="labels">Employee Location :
                        <input type="text" name="Location" value={employee.Location}
                        onChange={changeEmployeeInfo}></input>
                </label>
                <label className="labels">Employee Salary : 
                        <input type="text" name="Salary" value={employee.Salary}
                        onChange={changeEmployeeInfo}></input>
                </label>
            </p>
            <p>
                Employee ID is : <b>{employee.Id}</b>, Name is : <b>{employee.Name}</b> ,
                Location is : <b>{employee.Location}</b> and Salary is : <b>{employee.Salary}</b>
            </p>
        </div> 
        <Salary salary={employee.Salary} onSalaryChange={changeEmployeeInfo}/>        
        </>
      )
}