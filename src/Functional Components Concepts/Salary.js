import React from 'react';

export default function Salary ({onSalaryChange, salary}) {
    function changeSalary(e){
        onSalaryChange(e);    
      }
    
    return (
        <div className = 'box'>    
        <h2>Salary Functional Component...</h2> 
        <hr className = "line"></hr>   
          {/* <p>
              <label>Employee ID :
                  <input type="text" name="Id" value={employee.Id}
                  onChange={changeEmployeeInfo}></input>
              </label>
          </p>
          <p>
              <label>Employee Name : 
                      <input type="text" name="Name" value={employee.Name} 
                      onChange={changeEmployeeInfo}></input>
              </label>
          </p>
          <p>
              <label>Employee Location :
                      <input type="text" name="Location" value={employee.Location}
                      onChange={changeEmployeeInfo}></input>
              </label>
          </p> */}
          <p>
              <label>Employee Salary : 
                      <input type="text" name="Salary" value={salary}
                      onChange={changeSalary}></input>
              </label>
          </p>
      </div> 
    )
}