import React from 'react';
import EmployeeInfoComponent from './EmployeeInfo';
import './style.css';

export default function FunctionaComponent () {
     return (
        <div>
        <h2>Functional Component...UseEffect Hooks Implementation</h2> 
        <EmployeeInfoComponent />
        </div>
     )
}
 