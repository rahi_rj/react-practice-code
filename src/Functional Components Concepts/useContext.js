import React, {useState, useContext } from "react";
import './style.css'


const employeeContext = React.createContext();

export default function FunctionalComponent(){

  const [employee]=useState({Id:101,Name:'Rahul', Location:'Bangalore', Salary:12345});
  return(
    <div>
      <h1>Use Context Hooks...</h1>
      <hr></hr>
      <h2>1st Level Component...</h2>
      <employeeContext.Provider value={employee}>
          <Employee></Employee>
      </employeeContext.Provider>      
    </div>
  );
}

function Employee(){
  let context=useContext(employeeContext);

  return(
    <div className='box'>
      <h2>Employee Component...2nd Level</h2>
      <p>
        <label>Employee ID : <b>{context.Id}</b></label>
      </p>
      <p>
        <label>Employee Name : <b>{context.Name}</b></label>
      </p>
      <Salary></Salary>
    </div>
  );
}

function Salary(){
  let context=useContext(employeeContext);

  return(
    <div className='box'>
      <h2>Salary Component...3rd Level</h2>
      <p>
        <label>Employee ID : <b>{context.Id}</b></label>
      </p>
      <p>
        <label>Employee Salary : <b>{context.Salary}</b></label>
      </p>
    </div>
  );
}