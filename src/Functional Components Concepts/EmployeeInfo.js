import React, {useState, useEffect } from "react";
import {Table} from 'react-bootstrap'

export default function EmployeeInfoComponent(){
  const [employees,setEmployees]=useState([]);
  const [searchText, setSearchText]=useState('');

  useEffect(()=>{
    //alert('We are in useEffect function');
     let info = [
        {
            "Id": "M1044334",
            "Name": "Rahi",
            "Location": "Bangalore",
            "Salary": 5000
        },
        {
            "Id": "M1044335",
            "Name": "Rahul",
            "Location": "Bangalore",
            "Salary": 6000
        }
     ]
     if (searchText !== '' && searchText !== null)
    { info = info.filter( x=> x.Name === searchText); }
       setEmployees(info);
    // },[]); 
    },[searchText]);   // dependency on search text variable bcz it rerenders the component

    // Note:- If you want to run an effect only once (on mount and unmount), you can pass an empty array ([]) as a second argument.
    // This tells React that your effect doesn’t depend on any values from props or state, so it never needs to re-run.
    // It also can be said as dependency to the effect

    function onSearchTextChange(e){
        setSearchText(e.target.value);
    }
  
  return(
    <div className = 'box'>
      <h2>Employees Data...</h2>

      <p>
        <label>Search By Name : <input type="text" value={searchText}
                                onChange={onSearchTextChange}></input></label>
      </p>  
      <hr className = "line"></hr>   
      <Table striped bordered hover  variant="dark">
        <thead>
          <tr>
            <th>Employee Id</th>
            <th>Employee Name</th>
            <th>Employee Location</th>
            <th>Employee Salary</th>
          </tr>
        </thead>
        <tbody>
          {employees.map(emp=>(
            <tr key={emp.Id}>
              <td>{emp.Id}</td>
              <td>{emp.Name}</td>
              <td>{emp.Location}</td>
              <td>{emp.Salary}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  )
}