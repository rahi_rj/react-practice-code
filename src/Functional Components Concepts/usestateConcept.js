import React, { useState } from 'react';
import './style.css';
import EmployeeComponent from './Employee';
import './style.css'

export default function FunctionalComponent () {    

    return(
        <div>
        <h2>Functional Component...UseState Hooks Implementation</h2> 
        <EmployeeComponent />
        </div>
      )
}