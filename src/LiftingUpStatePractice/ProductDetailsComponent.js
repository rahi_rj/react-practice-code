import React from 'react';
import './Style.css';

export default class ProductDetailsComponent extends React.Component{

    constructor(props) {
  
      super(props);   
  
   } 
  
   handleChange=e=>{
  
     this.props.onQuantityChange(e.target.value);
  
   }
  
   render(){  
     return (  
       <div className= "box">  
         <h2>Order Information...</h2>  
         <p>  
           <label>Product Name :   
             <select>  
               <option value="Product-1">Product-1</option>  
               {/* <option value="Product-2">Product-2</option>  
               <option value="Product-3">Product-3</option>   */}
             </select> </label>  
         </p>  
         <p>  
           <label>Enter Quantity : <input type="text"  value={this.props.quantity}   
           onChange={this.handleChange}></input></label>  
         </p>  
       </div>  
     );  
   }  
}
  