import React from 'react';
import ProductDetailsComponent from './ProductDetailsComponent';
import SummaryComponent from './SummaryComponent';
import AddressComponent from './AddressComponent';
import './Style.css'

export default class OrderComponent extends React.Component{

    constructor(props) {
  
      super(props);   
  
      this.state={quantity:'', address:''};
  
   }
  
   orderInfoChanged=(val)=>{
  
     this.setState({quantity:val});
  
   }  
  
   addressChanged=(val)=>{
  
    this.setState({address:val});
  
  }
  
   render(){
  
     return(
  
      <div className="order">
  
      <h1>Welcome to Product Order Screen...</h1>
  
      <ProductDetailsComponent quantity={this.state.quantity} 
  
      onQuantityChange={this.orderInfoChanged}></ProductDetailsComponent>
  
      <AddressComponent address={this.state.address} 
  
      onAddressChange={this.addressChanged}></AddressComponent>
  
      <SummaryComponent quantity={this.state.quantity} address={this.state.address} 
  
      onQuantityChange={this.orderInfoChanged}></SummaryComponent>
  
    </div>
  
     );   
  
   }
  
  }