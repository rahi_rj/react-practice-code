import React from 'react';
import Salary from './Salary';
import {Table} from 'react-bootstrap';
import './Employee.css';

export default class Employee extends React.Component{

    constructor(props){
      super(props);
      this.state = {
        id:this.props.details.Id,
        name:props.details.Name,
        location:props.details.Location,
        salary:this.props.details.Salary,
        updatedSalary: "Not Updated Yet..."
      }
    }  

    getUpdatedSalary = (salary) => {
        this.setState({updatedSalary:salary});
              
    }

    render(){
        return (
            <>
            <div className="tableData">
                <div>
                <h2>Employee Details...</h2>
                </div>
                <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                    <th> Employee Id</th>
                    <th> Employee Name</th>
                    <th> Employee Location</th>
                    <th> Employee Salary</th>
                    <th> Employee Updated Salary</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td>{this.props.details.Id}</td>
                    <td>{this.props.details.Name}</td>
                    <td>{this.props.details.Location}</td>
                    <td>{this.props.details.Salary}</td>                    
                    <td>{this.state.updatedSalary}</td>
                    </tr>
                </tbody>
             </Table> 
             </div>
             <Salary BasicSalary={this.props.details.BasicSalary} HRA={this.props.details.HRA} SpecialAllowance={this.props.details.SpecialAllowance} onSalaryChanged={this.getUpdatedSalary}></Salary>
            </>
    
            )
    }
  }