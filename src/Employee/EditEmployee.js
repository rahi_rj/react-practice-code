import React, {Component} from 'react'
import './Employee.css'

export default class EditEmployee extends Component {
    constructor(props){
        super(props)
        this.state={
            id:this.props.employeeDetails.Id,
            name:this.props.employeeDetails.Name,
            location:this.props.employeeDetails.Location,
            salary:this.props.employeeDetails.Salary
          };
    }

    updateEmployee=()=>{
        let details ={
            id:this.refs.Id.value,
            name:this.refs.Name.value,
            location:this.refs.Location.value,
            salary:this.refs.Salary.value,
        }
        this.props.onEmployeeChanged(details);
    }


    render() {
        return (
        <div className = "box">
        <h2>Edit Employee Details...</h2>
        <hr className = "line"></hr>
        <div>
                <span className="spans">Employee Id :</span><input type="text" defaultValue={this.state.id} ref="Id"/>
                <span className="spans">Employee Name :</span> <input type="text" defaultValue={this.state.name} ref="Name" />
                <span className="spans">Employee Location :</span> <input type="text" defaultValue={this.state.location} ref="Location" /> 
                              
        </div>
        <div className="employeeSal">
                <span className="employeespans">Salary :</span> <input type="text" defaultValue={this.state.salary} ref="Salary" />  
        </div>
        <div>
                <button className="add" onClick={this.updateEmployee}>Update Employee</button>
        </div>
        </div>
      )

    }
}