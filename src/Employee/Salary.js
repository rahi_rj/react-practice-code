import React from 'react';
import './Employee.css'

export default class Salary extends React.Component{
    constructor(props){
      super(props);
      this.state={
        basic:this.props.BasicSalary,
        hra:this.props.HRA,
        sal:this.props.SpecialAllowance
      };
    }

    updateSalary=()=>{
        let salary=parseInt(this.refs.BasicSalary.value)+parseInt(this.refs.HRA.value)+ parseInt(this.refs.SpecialAllowance.value);
        this.props.onSalaryChanged(salary);
    }


    render(){
      return (
      <div className = "box">
        <h2>Enter Salary Details...</h2>
        <hr className = "line"></hr>
        <div className="inputfields">
                <span className="spans">Basic Salary :</span><input type="number" defaultValue={this.state.basic} ref="BasicSalary"/>
                <span className="spans">HRA :</span> <input type="number" defaultValue={this.state.hra} ref="HRA" />
                <span className="spans">Special Allowance :</span> <input type="number" defaultValue={this.state.sal} ref="SpecialAllowance" />                
        </div>
        <div>
                <button className="add" onClick={this.updateSalary}>Update Salary</button>
        </div>
      </div>
      )
    }
    
  }