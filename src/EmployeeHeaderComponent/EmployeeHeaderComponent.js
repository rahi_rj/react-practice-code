import React, {Component} from 'react';
import './EmployeeHeaderComponent.css';
import Employee from '../Employee/Employee'
import EditEmployee from '../Employee/EditEmployee'

export default class EmployeeHeaderComponent extends Component {
    constructor(props){
        super(props)
        this.state= {
            Id: "M1044334",
            Name:"Rahul Kumar",
            Location:"Bangalore", 
            Salary:50000,
            BasicSalary:25000, 
            HRA:10000, 
            SpecialAllowance:15000
    }
    this.getUpdatedEmployee = this.getUpdatedEmployee.bind(this)
    }
    
    /**
     * method to receive data from child and set them to state
     * @param {*} data 
     */
    getUpdatedEmployee(data){
        this.setState({
            Id: data.id,
            Name:data.name,
            Location:data.location, 
            Salary:data.salary, 
        })
    }

    render() {
     return(
        <>
         <div>
         <Employee details = {this.state} />
         <EditEmployee employeeDetails = {this.state} onEmployeeChanged={this.getUpdatedEmployee} />
         </div>  
        </>  
     );
    }
}