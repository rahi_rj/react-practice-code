import React from 'react';
import './HeaderComponent.css';

export default class HeaderComponent extends React.Component {
     render() {
         return (
        <div className="header">
            <h1>My React Learning Application</h1>
         </div>
         )
     }
}