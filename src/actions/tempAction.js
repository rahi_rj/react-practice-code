// "updateName" is an action creator which is a function that creates and returns an action object. We typically use these so we don't have to write the action object by hand every time:
export const updateName = name => (
    {
        type : 'UPDATE_NAME', payload : name
    }
)