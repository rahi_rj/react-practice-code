import React from 'react';
import { useState, useEffect } from 'react';
import {Table} from 'react-bootstrap';
import './routing.css';

function ParentsInfo() {
    
    const [parent,setParent]=useState([]);

    useEffect(()=> {
        let info = [
            {
                "Id": "M1044334",
                "ParentsName": "Rahul Jha",
                "Contact": "9876123456"
            }
         ]
         setParent(info);
    },[])
  
      return (
        <div className="tableData">
          <h2>Personal Information...</h2>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Student Id</th>
                <th>Parents Name</th>
                <th>Contact</th>
              </tr>
            </thead>
            <tbody>
            {parent.map(element => (
              <tr key={element.Id}>
                <td>{element.Id}</td>
                <td>{element.ParentsName}</td>
                <td>{element.Contact}</td>
                </tr>
            ))}
            </tbody>
          </Table>
        </div>
        );
}

export default ParentsInfo
