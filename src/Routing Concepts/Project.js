import React from 'react';
import { useState, useEffect } from 'react';
import {Table} from 'react-bootstrap';
import './routing.css';

function Project() {
    
  const [projects,setProjects]=useState([]);
    useEffect(()=> {
        let info = [
            {
                "Id": "P01",
                "Name": "Angular"
            },
            {
                "Id": "P02",
                "Name": "React"
            }
         ]
         setProjects(info);
    },[]);
  
      return (
        <div className='tableData'>
          <h2>Projects Details...</h2>

          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
            {projects.map(element => (
              <tr key={element.Id}>
                <td>{element.Id}</td>
                <td>{element.Name}</td>
                </tr>
            ))}
            </tbody>
          </Table>
          
        </div>
        );
}

export default Project