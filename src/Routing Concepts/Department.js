import React from 'react';
import { useState, useEffect } from 'react';
import {Table} from 'react-bootstrap';
import './routing.css';

function Department() {
    
  const [departments,setDepartments]=useState([]);
    useEffect(()=> {
        let info = [
            {
                "Id": "D01",
                "Name": "CSE"
            },
            {
                "Id": "D02",
                "Name": "IT"
            }
         ]
         setDepartments(info);
    },[]);
  
      return (
        <div className="tableData">
          <h2>Departments Data...</h2>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
              </tr>
            </thead>
            <tbody>
            {departments.map(element => (
              <tr key={element.Id}>
                <td>{element.Id}</td>
                <td>{element.Name}</td>
                </tr>
            ))}
            </tbody>
          </Table>
        </div>
        );
}

export default Department