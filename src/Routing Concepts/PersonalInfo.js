import React from 'react';
import { useState, useEffect } from 'react';
import {Table} from 'react-bootstrap';
import './routing.css';

function PersonalInfo() {
    
    const [student,setStudent]=useState([]);

    useEffect(()=> {
        let info = [
            {
                "Id": "M1044334",
                "Name": "Rahul Jha",
                "Location": "Bangalore"
            }
         ]
         setStudent(info);
    },[])
  
      return (
        <div className="tableData">
          <h2>Personal Information...</h2>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Location</th>
              </tr>
            </thead>
            <tbody>
            {student.map(element => (
              <tr key={element.Id}>
                <td>{element.Id}</td>
                <td>{element.Name}</td>
                <td>{element.Location}</td>
                </tr>
            ))}
            </tbody>
          </Table>
        </div>
        );
}

export default PersonalInfo