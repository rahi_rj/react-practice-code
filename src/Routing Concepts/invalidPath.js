import React from 'react';
import './routing.css'

function InvalidPath() {
    return (
        <h2 className="errorText">Page Not Found....Invalid Path</h2>
    )
}

export default InvalidPath