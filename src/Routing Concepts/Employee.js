import React from 'react';
import { useState, useEffect } from 'react';
import {Table} from 'react-bootstrap';
import './routing.css';

function Employee() {
    
    const [employees,setEmployees]=useState([]);

    useEffect(()=> {
        let info = [
            {
                "Id": "M1044334",
                "Name": "Rahi",
                "Location": "Bangalore",
                "Salary": 5000
            },
            {
                "Id": "M1044335",
                "Name": "Rahul",
                "Location": "Bangalore",
                "Salary": 6000
            }
         ]
         setEmployees(info);
    },[])
  
      return (
        <div className="tableData">
          <h2>Employees Data...</h2>
          <Table striped bordered hover variant="dark">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Location</th>
                <th>Salary</th>
                <th>Action(s)</th>
              </tr>
            </thead>
            <tbody>
            {employees.map(element => (
              <tr key={element.Id}>
                <td>{element.Id}</td>
                <td>{element.Name}</td>
                <td>{element.Location}</td>
                <td>{element.Salary}</td>
                <td>
                <a href={'/employee/'+element.Id}>Edit</a>
                </td>
                </tr>
            ))}
            </tbody>
          </Table>
        </div>
        );
}

export default Employee