import React from 'react';
import {NavLink, Link, Switch, Route} from 'react-router-dom';
import PersonalInfo from './PersonalInfo';
import ParentsInfo from './ParentsInfo';
import './routing.css'

function Student(props) {
    return (
        <div className="tableData">
            <h2>Student Details</h2>
            <hr></hr>
            <div className="links">
                <Link to={"/student/"}>Personal Info</Link> &nbsp;&nbsp;
                <NavLink to={"/student"+"/parents"} activeClassName="testClass">Parents Info</NavLink>
            </div>
            <hr className = "line"></hr>
            <Switch>

                {/* <Route exact path="/student" component={PersonalInfo}></Route> */}
                <Route exact path={props.match.url} component={PersonalInfo}></Route>
                {/* <Route path="/student/parents" component={ParentsInfo}></Route> */}
                <Route path={props.match.url+"/parents"} component={ParentsInfo}></Route>
                
            </Switch>           
        </div>
    )
}

export default Student