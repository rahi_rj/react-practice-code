import React from 'react';
import {NavLink, Link, Switch, Route} from 'react-router-dom';
import Employee from './Employee';
import EditEmployee from './EditEmployee'
import Department from './Department';
import Project from './Project';
import Student from './Student';
import InvalidPath from './invalidPath'
import './routing.css'

function Routing() {
    return (
        <div>

        <h2 className="heading">Routing Implementation...</h2>

        <button className='btnLink'><Link to="/">Employees</Link></button>
        <button className='btnLink'><NavLink to="/departments" activeClassName="testClass">Departments</NavLink></button>
        <button className='btnLink'><NavLink to="/projects" activeClassName="testClass">Projects</NavLink></button>
        <button className='btnLink'><NavLink to="/student" activeClassName="testClass">Students</NavLink></button>
        
        <hr className = "line"></hr>
        
        <Switch>
        {/* <Route path="/employees" component={Employee}></Route> */}
        <Route exact path="/" component={Employee}></Route>
        <Route path="/employee/:id" component={EditEmployee}></Route>
        <Route path="/departments" component={Department}></Route>
        <Route path="/projects" component={Project}></Route>
        <Route path="/student" component={Student}></Route>
        {/* <Route path="/departments" component={Employee}></Route> */}
        <Route component={InvalidPath}></Route>
        </Switch>

      </div>
    )
}

export default Routing;

// Note: 
// 1. React Router Provides another Component Called as NavLink which is a  special type of <Link> that can style itself as “active” when its to property value matches the current location.
// 2. When a <Switch> is rendered, it searches through its children <Route> elements to find one whose path matches the current URL. When it finds one, it renders that <Route> and ignores all others.
// 3. Order of the Routes do play important role.