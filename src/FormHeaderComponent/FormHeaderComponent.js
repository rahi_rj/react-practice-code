import React, {Component} from 'react';
import './FormHeaderComponent.css';
import AddEmployee from '../FormPractice/AddEmployee';

export default class FormHeaderComponent extends Component {
    constructor(props){
        super(props)
    }

    /**
     * render method
     */
    render() {
     return(
        <>
         <div>            
             <AddEmployee />
         </div>  
        </>  
     );
    }
}