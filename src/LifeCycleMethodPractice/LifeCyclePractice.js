import React from 'react';
import LifeCyclePractical from './LifeCyclePractical'

/**
 * Class to test life cycle hooks with example
 */
export default class LifeCyclePractice extends React.Component {
  constructor(props) {
     super(props);     
     this.state = {
        data: 0
     }
     this.setNewNumber = this.setNewNumber.bind(this)
  };

  setNewNumber() {
     this.setState({data: this.state.data + 1})
  }

  render() {
     return (
        <div>
           <button onClick = {this.setNewNumber}>INCREMENT</button>
           <LifeCyclePractical myNumber = {this.state.data} />
        </div>
     );
  }
}